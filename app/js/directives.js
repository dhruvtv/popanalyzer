'use strict';

/* Directives */

app.directive('chosen',function(){
    var linker = function(scope,element,attrs) {
        var list = attrs['chosen'];

        scope.$watch(list, function(){
            element.trigger("chosen:updated");
        });

        element.chosen();
    };

    return {
        restrict:'A',
        link: linker
    }
})
