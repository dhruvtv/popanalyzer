'use strict';

var app = angular.module('popAnalyzer', []);

// This configures routes and assigns each view with a route controller
app.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/', {templateUrl: 'partials/analyze.html', controller: 'AppController'});
}]);
