'use strict';

/* Services */

// Service that creates Highcharts chart
app.service('chartService', function () {

    init();

    function init() {
        Highcharts.setOptions({
            lang: {
                thousandsSep: ""
            }
        });
    }

    this.createChart = function(startYear, selectionEvent) {
        var chartOptions = {
            chart: {
                renderTo: 'chartContainer',
                zoomType: 'xy',
                events: {
                    selection: selectionEvent
                }
            },
            title: {
                text: 'Population of the world',
                x: -10 //center
            },
            subtitle: {
                text: 'Source: worldbank.org',
                x: -10
            },
            xAxis: {
                title: {
                    text: 'Year'
                }
            },
            yAxis: {
                title: {
                    text: 'Population'
                },
                plotLines: [
                    {
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }
                ]
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            plotOptions: {
                series: {
                    pointStart: startYear
                }
            },
            series: []
        }

        return new Highcharts.Chart(chartOptions);
    }
});
