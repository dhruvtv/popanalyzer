'use strict';

/* Controllers */

// This controller:
// 1. Retrieves data from the XML file, transforms it
// 2. Renders the data into a table
// 3. Let users visualize it with charts

app.controller('AppController', function ($scope, $http, chartService) {

    var lastSelectedCountryCodes = []; // Keep track to compute added/removed country in HTML multi select box
    var countryDict = {}; // Each entry is COUNTRY_CODE : { code, name, entries (years) }

    $scope.getData = function () {
        $http.get('data/world-pop.xml').success(function(data) {
            transformXmlToCountryData(data);
            initScopeVariables();
        })
    }

    function transformXmlToCountryData(xml) {
//        var countries = {};
        // Parse the XML using jQuery's XML parser
        var xmlDoc = $.parseXML(xml);
        var record; // each record in the XML file (corresponds to a country-year-population entry)
        var fields; // the fields (country, year, population) in each record
        var country, countryCode, countryName, year, value; // temp vars
        var startYear = 2100, endYear = 0; // will compute start and end years from on input data

        // There are about 12,000 of these entries
        $(xmlDoc.firstChild.firstElementChild).children().each(function (index) {
            record = $(this);
            fields = record.children();

            // Expression matching these fields takes forever
//            country = record.children("field[name = 'Country or Area']").first();
//            year = parseInt(record.children('field[name = "Year"]').first().val());
//            value = parseFloat(record.children('field[name = "Value"]').first().val());

            // so, trust the field order/format
            country = fields.eq(0);
            countryCode = country.attr('key');
            countryName = country.text();

            year = parseInt(fields.eq(2).text());
            if (year < startYear) startYear = year; // make startYear the oldest year encountered
            if (year > endYear) endYear = year; // make endYear the newest year encountered
            value = parseFloat(fields.eq(3).text()); // population value.
            value = isNaN(value) ? null : value;

            if (!Object.has(countryDict, countryCode)) { // encountered a new country
                // add an entry in dict with countryCode as key (unique)
                countryDict[countryCode] = {
                    code: countryCode,
                    name: countryName,
                    entries: []
                }
            }

            // add a year-population entry to a country record
            countryDict[countryCode].entries[year] = value;
        });

        // Final start and end years in the data
        $scope.startYear = startYear;
        $scope.endYear = endYear;
    }

    function initScopeVariables() {

        // Range of year values - to set as table column headers
        // ngRepeat can't handle arr[value_type], converting to arr[reference_type]
        $scope.years = Number.range($scope.startYear, $scope.endYear).every(1).map(function (n) {
            return { value: n};
        });

        // Again, converting the country records into an array for Angular
        // (can't figure out how to handle boolean exp with objects)
        $scope.countries = Object.values(countryDict);

        // Starting out by displaying just 20 items in the table and select box,
        // because Angular takes forever to render 250+ rows with 50 columns each.
        // Will ask the user to load more as he needs (Is this OK?)
        $scope.totalDisplayed = 20;

        // Initializing the log of selected points in chart (angular uses this)
        $scope.selectionLog = [];

        // Start out with a line chart, can change this to 'scatter'
        $scope.chartType = "line";

        // Using the Chosen jQuery plugin to display a MUCH better multi-select box. (See directive)
        // Every time we load more countries into it, Chosen needs to rebuild its index.
        $scope.$watch('filteredCountries.length', function () {
            $('#chosen-countries').trigger('chosen:updated');
        })

        // Create a new Highcharts chart passing it the startYear (X axis) and what to do at a selection event
        $scope.chart = chartService.createChart($scope.startYear, $scope.areaSelected);
//        draw();

        // Tells Angular to display the table and chart views
        $scope.loadingDone = true;
    }

    /**
     * Load 20 more countries into the table and the select box.
     */
    $scope.loadMore = function () {
        // Updating this filter variable causes ngRepeat to render the rest of entries
        $scope.totalDisplayed += 20;
    }

    /**
     * Load all the countries into the table and the select box. Takes a few seconds for Angular to render them.
     */
    $scope.loadAll = function () {
        $scope.totalDisplayed = $scope.countries.length;
    }


    /**
     * Chart related methods
     */

    /**
     * Change chart type between "line" and "scatter". Technically, to whatever is in $scope.chartType.
     */
    $scope.changeChartType = function () {
        // Clear previous selection of points (and log) because they all will be destroyed and redrawn now.
        clearPreviousSelection();

        for (var i = 0; i < $scope.chart.series.length; i++)
            // Update recreates and redraws all points in this series with the new chart type
            $scope.chart.series[i].update({
                type: $scope.chartType
            });
    }

    /**
     * Add or remove a country from chart based on user selection
     */
    $scope.addOrRemoveCountry = function () {
        /** There is no good (elegant) way to check the last selected/deselected item from an HTML multi-select box.
         * So, we've to resort to this dirty trick of keeping track of last state and computing
         * changedItem = delta(currentState, lastState) where
         */
        var delta = $scope.selectedCountryCodes.length - lastSelectedCountryCodes.length;
        if (delta > 0) // item added
            addCountryToChart();
        else if (delta < 0) // item deleted
            removeCountryFromChart();
    }

    function addCountryToChart() {
        var addedCountryCode = $scope.selectedCountryCodes.subtract(lastSelectedCountryCodes).first();
        // Theoretically, the size of delta should be 1, so getting the first element works.

        // Add it as a series to the chart
        $scope.chart.addSeries({
            id: addedCountryCode, // can access series by this id - chart.get(id)
            name: countryDict[addedCountryCode].name,

            // country.entries is an array with indices between startYear and endYear, fixing that.
            // some country/year entries have NaN values, massaging them so Highcharts draws the available years.
            data: countryDict[addedCountryCode].entries
                .slice($scope.startYear, $scope.endYear + 1).map(function(n) { return isNaN(n) ? { y: null} : n;}),

            // explicitly specifying each series to be drawn as <type>
            type: $scope.chartType
        });

        // Update state
        lastSelectedCountryCodes.push(addedCountryCode);
    }

    function removeCountryFromChart() {
        var removedCountryCode = lastSelectedCountryCodes.subtract($scope.selectedCountryCodes).first();

        var series = $scope.chart.get(removedCountryCode);

        // Clearing the selection log for this series(country), if any
        $scope.selectionLog.remove(function(logEntry) {
            return logEntry.seriesRef == series; // remember, each log entry has a reference to series?
        });

        // Remove the series
        series.remove();

        // Update state
        lastSelectedCountryCodes.remove(removedCountryCode);
    }

    /**
     * What happens when you rubber-band select an area on the chart
     * @param event
     */
    $scope.areaSelected = function(event) {
        // Prevent the default Highcharts behavior of zooming
        event.preventDefault();

        // Selection range
        var xInterval = new Interval1D(event.xAxis[0].min, event.xAxis[0].max);
        var yInterval = new Interval1D(event.yAxis[0].min, event.yAxis[0].max);

        // Clear the previous selection
        clearPreviousSelection();

        // Check if a point is within the selection (see below) and *select it*.
        function selectionContains(point) {
            var contains = xInterval.contains(point.x) && yInterval.contains(point.y);
            // As a side-effect, select the point on the chart
            if (contains) point.select(true, true); // select and accumulate
            return contains;
        }

        // Go through each series' and see if any of its points fall in selection range
        for (var i = 0; i < $scope.chart.series.length; i++) {
            var series = $scope.chart.series[i];
            var anySelected = false;

            // Check if any point falls in selection range, if so, select
            var selectedPoints = series.data.filter(function (point) {
                return selectionContains(point);
            })

            // Add to Selection Log to be displayed to user
            if (selectedPoints.length > 0) {
                $scope.selectionLog.push({
                    seriesRef: series, // get a reference to series,
                    // because when the series is deleted, we need to delete this entry,
                    // and series is the only unique handle we can get at this point.
                    seriesName: series.name,
                    selectedPoints:selectedPoints
                });
            }
        }

        // Need to push selectionLog to UI.
        $scope.$apply();
    }

    /**
     * When rubber-band-selecting a new set of points, or switching the chart type (thereby redrawing all points)
     * need to clear old selected points from chart, and clear the log.
     */
    var clearPreviousSelection = function() {
        // Go through each log entry (series) and its selected points, and *deselect them* on chart.
        Object.values($scope.selectionLog).each(function(logEntry) {
            logEntry.selectedPoints.each(function(point) {
                point.select(false);
            })
        })

        // Clear the log.
        $scope.selectionLog = [];
    }

    // Helper objects

    /**
     * Define an interval with a min value and a max value
     * @param min
     * @param max
     * @constructor
     */
    var Interval1D = function(min, max) {

        /**
         * Check if the interval contains the given point
         * @param point
         * @returns true if it does
         */
        this.contains = function(point) {
            return point >= min && point <= max;
        }
    }
});