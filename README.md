# Population Analyzer

An HTML5 app that lets you analyze the world's population between 1960 and 2011 using tablular and chart views.

It is hosted [here](http://popanalyzer.nfshost.com).

## How it works

It reads the population data from an xml file (already on the server), parses it and presents it as a table.
You can also pick specific countries/regions and draw line and scatter charts.

## About the code

It is built using Angular.js and Bootstrap.

## Notes

If you're trying the app out, please be a bit patient. It loads around 12500 entries so it can take 10-15 seconds.
